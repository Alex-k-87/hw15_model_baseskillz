<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    private $id;
    private $name;

    /**
     * Category constructor.
     * @param $id
     * @param $name
     */
    public function __construct($id, $name)
    {
        $this->setId($id);
        $this->setName($name);
    }

    public static function find($id)
    {
        $pdo = new \PDO('mysql:host=mysql;dbname=docker','root','password' );

        $sql = 'SELECT * FROM categories WHERE id = :id';

        $stmt = $pdo->prepare($sql);

        $stmt->bindValue(':id', $id);

        $stmt->execute();

        $data = $stmt->fetch(\PDO::FETCH_ASSOC);

        return new self($data['id'], $data['name']);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

}
