<?php

require_once '../vendor/autoload.php';
require_once '../config/eloquent.php';

use App\Model\Category;

$category = Category::withTrashed()->find(2);
$category->restore();